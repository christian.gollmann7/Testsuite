#-------------------------------------------------------------------------------
#              Main CMakeLists.txt file for CFS++ Testsuite.
# 
# To use testsuite please specify the base path to this file in cmake for CFS++.
#-------------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.10)

# check if we run from within cfs (i.e. testsuite is included)
if(NOT PROJECT_NAME)
  set(CMAKE_MAKE_PROGRAM "none") # we do not need any
  project(TESTSUITE NONE)
  set(CFS_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}" CACHE PATH "location of cfs installation to test")
  # find the binary
  # message("CMAKE_CURRENT_BINARY_DIR=${CMAKE_CURRENT_BINARY_DIR}")
  if(EXISTS "${CFS_BINARY_DIR}/bin/cfs")
    # run cfs and prinf version info
    execute_process(COMMAND ${CFS_BINARY_DIR}/bin/cfs --version --noColor
                    RESULT_VARIABLE res
                    OUTPUT_VARIABLE cfs_version_output)
    message(STATUS "running: cfs --version\n${cfs_version_output}")
    # run cfs to get build config information
    execute_process(COMMAND "${CFS_BINARY_DIR}/bin/cfs" "--dependencies" "${CMAKE_CURRENT_BINARY_DIR}/config.cmake" 
	  RESULT_VARIABLE res 
	  OUTPUT_VARIABLE STDOUT)
    if(NOT ${res})
      set(CFS_BUILD_CONFIG_FILE "${CMAKE_CURRENT_BINARY_DIR}/config.cmake")
      set(TESTSUITE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    else()
      message(ERROR "res=${res}, fail: ${out}")
    endif()
  else(EXISTS "${CFS_BINARY_DIR}/bin/cfs") # could not find executable
    message(ERROR "cannot find cfs binary: correct CFS_BINARY_DIR !")
  endif()
endif(NOT PROJECT_NAME)

message(STATUS "Testsuite will be testing: ${CFS_BINARY_DIR}/bin/cfs")

# CFS_BUILD_CONFIG_FILE can be set
#  a) above if Testsuite is used standalone
#  b) via -D if one wants ot override defaults for testing. This is used in pipeline tests to carry over the build config between stages
if(CFS_BUILD_CONFIG_FILE)
  message(STATUS "Testsuite is loading CFS build config from file '${CFS_BUILD_CONFIG_FILE}' to define tests")
  include("${CFS_BUILD_CONFIG_FILE}")
endif()

# Include CMake module for testing support
include(CTest)

# configure the testsuite

# set the number of thread to use for the testing runs. We set to > 1 to test parallelization. 
# The golden rule for fast tests ist minimal number of threads and testing parallel with 
# ctest -j N where N*TESTSUITE_THREADS=system threads (including hyperthreading)
# fastest would be TESTSUITE_THREADS=1, e.g. 2 min with ctest -j 8. TESTSUITE_THREADS=2 with ctest -j 4 is 3 minutes, then it becomes even slower.
set(TESTSUITE_THREADS "2" CACHE STRING "Number of threads to use for the cfs testing runs, i.e. use argument '-t TESTSUITE_THREADS' in cfs runs.")
# check if integer is set
if(NOT TESTSUITE_THREADS MATCHES "^[1-9][0-9]*$")
  message(FATAL_ERROR "TESTSUITE_THREADS must be integer valued.")
endif()

# set the mode of cfstool
set(TESTSUITE_CFSTOOL_MODE "relL2diff" CACHE STRING "The mode used to compare hdf5 files by cfstool.")
set_property(CACHE TESTSUITE_CFSTOOL_MODE PROPERTY STRINGS scalardiff L2diff relL2diff absL2diff)

# set the test timeout
set(TESTSUITE_TIMEOUT 300 CACHE STRING "timeout in s for each test in the testsuite (sets DART_TESTING_TIMEOUT)")
set(DART_TESTING_TIMEOUT "${TESTSUITE_TIMEOUT}" CACHE INTERNAL "=TESTSUITE_TIMEOUT") # see https://cmake.org/cmake/help/v3.3/manual/ctest.1.html#ctest-test-step
mark_as_advanced(TESTSUITE_TIMEOUT)

# check options
# check if we have dependent options set correctly
# In order to perform tests we need the CFSTOOL and HDF5!
if(NOT BUILD_CFSTOOL)
#    message(FATAL_ERROR "BUILD_TESTING=ON requires BUILD_CFSTOOL=ON.")
endif()

# Switch on support for testing
enable_testing()

# configure the dashboard and include config
# write the variables in DartConfiguration.tcl which is used by `ctest -D` or `make Experimental`
# https://github.com/Kitware/CMake/blob/master/Modules/DartConfiguration.tcl.in
set(DROP_SITE "movm.mi.uni-erlangen.de" CACHE STRING "URL of the CDash dashboard to submit test results")
mark_as_advanced(DROP_SITE)
set(DROP_LOCATION "/cdash/submit.php?project=CFS")
set(DROP_METHOD "http")
set(NIGHTLY_START_TIME "00:00:00 CET")
set(SUBMIT_URL "${DROP_METHOD}://${DROP_SITE}${DROP_LOCATION}") # for recent cmake versions
message(STATUS "submitting tests to: ${SUBMIT_URL}")
include(CTest) # to actually write DartConfiguration.tcl (again?)

# enbale some custom ctest settings
configure_file("CTestCustom.cmake.in" "${CMAKE_BINARY_DIR}/CTestCustom.cmake" @ONLY)

#-------------------------------------------------------------------------------
# Include some macros. Especially the one for generating test names out of
# directory names.
#-------------------------------------------------------------------------------
include("TestMacros.cmake")
#include("${CFS_SOURCE_DIR}/cmake_modules/distro.cmake") # TODO: check if this is necessary for something

# Set file name for configured version of CFS++ standard test macro (there is a .in version)
# * cfstool for comparisson of h5 files (default)
# * compare_info_xml.py for comparison of special info.xml stuff (eigenfrequencies) via parameter "info.xml"
set(CFS_STANDARD_TEST "${CFS_BINARY_DIR}/testsuite/StandardTest.cmake")
set(PYTHON_TEST "${CFS_BINARY_DIR}/testsuite/PythonTest.cmake")

# Define path to info.xml compare script (for eigenvalue and other stuff)
set(COMPARE_INFO_XML "${CMAKE_CURRENT_SOURCE_DIR}/compare_info_xml.py")

# python tests including matviz
set(MATVIZ_PY "${CFS_BINARY_DIR}/share/python/matviz.py")
set(BASECELL_PY "${CFS_BINARY_DIR}/share/python/basecell.py")

# Checks matviz.py results
set(COMPARE_IMAGES "${CMAKE_CURRENT_SOURCE_DIR}/compare_images.py")

# Enables tests of Python scripts e.g. via doctest
set(TESTSUITE_PYTHON ON CACHE BOOL "testing of python modules")
mark_as_advanced(PYTHON_EXECUTABLE)
# option to use tests which need 'compare_info_xml.py'
set(TESTSUITE_INFO_XML ON CACHE BOOL "Include tests which need 'compare_info_xml.py'")
mark_as_advanced(TESTSUITE_INFO_XML)

# check for python
if(TESTSUITE_PYTHON OR TESTSUITE_INFO_XML)
  # this is ignored if CFS has already been configured with python support
  if(PYTHON_EXECUTABLE)
    message(STATUS "PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE} ... used for python search of (Testsuite config)")
  endif()
  # look for python in custom paths, e.g. anaconda install
  find_program(PYTHON_EXECUTABLE NAMES python3 python PATHS "/share/programs/anaconda3/2020.02/bin" "/share/programs/anaconda3/latest/bin" "/share/programs/bin" DOC "interpreter to use for executing python tests" NO_DEFAULT_PATH)
  message(STATUS "find_program found python: ${PYTHON_EXECUTABLE}")
  # look for system python (first 3)
  find_package(PythonInterp 3)
  # check for python (2 as fallback)
  find_package(PythonInterp)
  # check for required python modules
  set(PythonModules_FIND_COMPONENTS "")
  # use the OPTIONAL_COMPONENTS to define our custom error message
  set(OPTIONAL_COMPONENTS lxml argparse os numpy h5py vtk scipy matplotlib)
  set(PythonModules_FIND_OPTIONAL_COMPONENTS ${OPTIONAL_COMPONENTS})
  # this will only work on the first execution with a set PYTHON_EXECUTABLE
  include("${CMAKE_CURRENT_SOURCE_DIR}/FindPythonModules.cmake")
  message(STATUS "PythonModules_FOUND=${PythonModules_FOUND}")
  message(STATUS "PythonModules_OPTIONAL_FOUND=${PythonModules_OPTIONAL_FOUND}")
  if(NOT PythonModules_OPTIONAL_FOUND) # report which ones are missong
    #message("${OPTIONAL_COMPONENTS}")
    foreach(module IN ITEMS ${OPTIONAL_COMPONENTS})
      #message(STATUS "${module} ${PythonModules_${module}_FOUND}")
      if(NOT ${PythonModules_${module}_FOUND})
	message(WARNING "Python module '${module}' missing!")
      endif()
    endforeach()
  endif()
  mark_as_advanced(PythonModules_DIR)
endif()

if(TESTSUITE_PYTHON AND PYTHON_EXECUTABLE MATCHES NOTFOUND)
  message(FATAL_ERROR "TESTSUITE_PYTHON=ON but python was not found: please set PYTHON_EXECUTABLE")
endif()

if(TESTSUITE_INFO_XML AND PYTHON_EXECUTABLE MATCHES NOTFOUND)
  message(FATAL_ERROR "TESTSUITE_INFO_XML=ON but python was not found: please set PYTHON_EXECUTABLE")
endif()

# there are no hysteresis tests?!
# set(TESTSUITE_HYSTERESIS ON CACHE BOOL "Include hysteresis tests (some may take long)")
#-------------------------------------------------------------------------------
# Set default tolerance for scalar difference tests using cfstool.
#-------------------------------------------------------------------------------
set(CFS_DEFAULT_EPSILON "1e-6")

# Subdirectory for normal test suite
add_subdirectory("TESTSUIT")

# Subdirectory for pypthon doctest, matviz visualization test suite and basecell
add_subdirectory("PYTHON")

# Configure CFS++ standard test macro.
configure_file("StandardTest.cmake.in" "${CFS_STANDARD_TEST}" @ONLY)

# python tests including matviz.py is used in the optimization group
configure_file("PythonTest.cmake.in" "${PYTHON_TEST}" @ONLY)
