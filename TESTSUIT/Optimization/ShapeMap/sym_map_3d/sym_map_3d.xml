<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
 
 <documentation>
    <title>ShapeMap with symmetric mapped 3d shape</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2018-03-05</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>Fabian Wein</references>
    <isVerified>no</isVerified>
    <description>Symmetry mapping of a single shape (the right is as the left and, half the variables).
    The design does not exactly look like one would expect the analytical solution.</description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region material="99lines" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load">
        <coord x="1" y="0.5" z="0.5"/>
      </nodes>
    </nodeList>
  </domain>


  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="left"> 
              <comp dof="x"/> 
              <comp dof="y"/> 
              <comp dof="z"/>
           </fix>
           <force name="load" >
             <comp dof="y" value="-1"/>
           </force>
           
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <cholmod/>
<!--           <lis> -->
<!--             <precond> -->
<!--               <ssor/> -->
<!--             </precond> -->
<!--             <solver> -->
<!--               <CG/> -->
<!--             </solver> -->
<!--           </lis> -->
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
    
  <optimization>
    <costFunction type="compliance" sequence="1" linear="false">
      <stopping queue="55" value="0.0001" type="relativeCostChange" maxHours="48"/>
    </costFunction>
    
    <constraint type="volume" bound="upperBound" value="0.5" design="density" linear="false"/>
    
    <constraint type="curvature" bound="upperBound" value="0.3/nx" design="node" linear="true"/>
    <constraint type="curvature" bound="upperBound" value="0.3/nx" design="profile" linear="true"/>
   
    <optimizer type="scpip" maxIterations="20">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="5e-8"/>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="shapeMap">
      <!-- we cannot achieve sufficient integration anyway - so make it faster --> 
      <shapeMap beta="60" overlap="max" enforce_bounds="false" integration_order="3" shape="linear">
       <center left_right_sym="mirror" >
         <node lower="0" upper="1" initial=".7" dof="y" />
         <node lower="0" upper="1" initial=".5" dof="z"/>
       </center> 
        <profile lower=".2" upper=".5" initial=".4"/>
      </shapeMap>
      
      <design name="density" initial=".5" physical_lower="1e-9" upper="1.0"/>

      <transferFunction type="identity" application="mech"/>
      <result value="shapeMapGrad" id="optResult_1" detail="node_a"/>
      <result value="shapeMapGrad" id="optResult_2" detail="node_b"/>
      <result value="shapeMapGrad" id="optResult_3" detail="profile"/>
      <result value="shapeMapIntOrder" id="optResult_4"  />
      
      <export save="last" write="iteration" density="false"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
</cfsSimulation>
