<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Harmonic Flat Plate</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2017-11-05</date>
        <keywords>
            <keyword>magneticEdge</keyword>
            <keyword>heatConduction</keyword>
            <keyword>transient</keyword>
        </keywords>
        <references> Prechtl "Grundlagen der Elektrotechnik Band 2" Bsp. A26.9 </references>
        <isVerified>no</isVerified>
        <description> This is a simple cylindric flat aluminium plate with diameter D in a magnetic
            field with magnetic flux density normal to the plate. The alternating magnetic field has
            a amplitude of 1T and for a frequency of 50Hz, the induced current at the outer diameter
            is 53,4A/mm². The analytic solution for the diameter-dependent current is \hat{J}(r) =
            0.25 \omega \gamma \hat{B} r D .
            Transient analysis with temperature-dependent electric conductivity
        </description>
    </documentation>
    <fileFormats>
        <input>
            <gmsh fileName="plateNew.msh"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d">
        <variableList>
            <var name="freq" value="5"/>
            <var name="B_amplitude" value="6E3"/>
        </variableList>
        <regionList>
            <region name="V_aluminium" material="NC52"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    
    <sequenceStep index="1">
        <analysis>
            <transient>
                <numSteps>10</numSteps>
                <deltaT>0.01</deltaT>
            </transient>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V_aluminium" polyId="Hcurl"  matDependIds="cond"/>
                </regionList>
                
                <matDependencyList>
                    <electricConductivity id="cond">
                        <coupling pdeName="heatConduction">
                            <quantity name="heatTemperature"/>
                        </coupling>
                    </electricConductivity>
                </matDependencyList>
                
                <bcsAndLoads>
                    <fluxDensity name="V_aluminium">
                        <comp dof="y" value="B_amplitude*sin(2*pi*freq*t)"/>
                    </fluxDensity>
                </bcsAndLoads>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotentialD1">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <regionResult type="magEnergy">
                        <allRegions outputIds="txt"/>
                    </regionResult>
                </storeResults>
            </magneticEdge>
            
            <heatConduction>
                <regionList>
                    <region name="V_aluminium" polyId="H1" nonLinIds="cond"/>
                </regionList>
                
                <nonLinList>
                    <heatConductivity id="cond"/>
                </nonLinList>
                
                
                <bcsAndLoads>
                    <heatSourceDensity volumeRegion="V_aluminium" name="V_aluminium">
                        <coupling pdeName="magneticEdge">
                            <quantity name="magJouleLossPowerDensity"/>
                        </coupling>
                    </heatSourceDensity>
                    <heatTransport name="S_aluminium" volumeRegion="V_aluminium" bulkTemperature="1" heatTransferCoefficient="0.1"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="heatFluxDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </heatConduction>
        </pdeList>
        
        
        <couplingList>
            <iterative>
                <convergence logging="yes" maxNumIters="10" stopOnDivergence="yes">
                    <quantity name="magJouleLossPowerDensity" value="1e-6"/>
                </convergence>
            </iterative>
        </couplingList>
        
        <linearSystems>
            <system >
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes">
                            <lineSearch/>
                            <incStopCrit> 1e-5</incStopCrit>
                            <resStopCrit> 1e-5</resStopCrit>
                            <maxNumIters> 20  </maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>
