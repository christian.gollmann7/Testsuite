%% Evaluate Maxwell slip BC

%% read in

eval_step = 15; % last step

[t,res_cell,coords_cell] = read_res('results_hdf5/MaxwellSlipBC2D_fine.cfs','y',0.04,'fluidMechVelocity');

%% sort

x_coords = zeros(length(coords_cell),1);
sorted_res_cell = cell(size(res_cell));

for ii=1:length(coords_cell)
    x_coords(ii) = coords_cell{ii}(1);
end

[sorted_x_coords, sorting_vec] = sort(x_coords);

for ii=1:length(coords_cell)
    sorted_res_cell{ii} = res_cell{sorting_vec(ii)};
end


%% evaluate

res = zeros(length(coords_cell),1);

for ii=1:length(coords_cell)
    dummy_vec = sorted_res_cell{ii};
    res(ii) = dummy_vec(eval_step);
end

%figure
%plot(sorted_x_coords,res)


sorted_x_coords_diff = sorted_x_coords(1:end-1)+diff(sorted_x_coords);
duydx = diff(res)./diff(sorted_x_coords);

%figure
%plot(sorted_x_coords_diff,duydx)

% the Maxwell BC prescribes -duy/dx*C*meanFreePath = u_t
disp(['Tangential velocity based on the normal derivative (nominal value): ' num2str(-duydx(end)*68e-9*1000) ' m/s'])
disp(['Actual tangential velocity (actual value): ' num2str(res(end)) ' m/s'])
