<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  
  <documentation>
    <title>2D closed magnetic circuit with hysteresis model</title>
    <authors>
      <author>Michael Nierla</author>
    </authors>
    <date>2019-06-18</date>
    <keywords>
      <keyword>magneticNodal</keyword>
      <keyword>hysteresis</keyword>
      <keyword>linesearch</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>
      Geometry:
      2D-quarter model of a Single-Sheet-Tester (SST), consisting of
      a thin material probe (made of hysteretic material), a thick 
      ferrite-core for closing the magnetic circuit, an excitation
      coil and a surrounding air region.
      
      Sketch:
      
      y-axis
      |
      | air
      |_____________
      |				      |
      |_ferrite_    |
      |_________|   |
      |         |   |
      |_coil____|   |
      |_________|___|
      |_probe_____|______ x-axis
      
      Simulation/Solution of non-linear system
      a) used hysteresis model: 
      Vector Preisach Model based on rotational operators; revised version (harder to invert due to angular distance)
      b) solution method for non-linear system:
      Quasi-Newton scheme, using Finite-Differences for estimating/approximating
      the Jacobian matrix, Jacobian is evaluated each iteration
      > Quasi-Newton is a B-Based formulation (i.e., local inversion of Preisach Model is
      required; see mat.xml); to obtain better starting point (Newton converges only locally) use initial fix-point steps (B-based)
      c) used linesearch method for globalization:
      inexact linesearch based on a non-monotonic Armijo-rule
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <mesh fileName="SSTRedux.mesh"/>
    </input>
    <output>
      <hdf5 id="hdf5"/>
      <text id="txtEntity" fileCollect="entity"/>
      <text id="txtTime" fileCollect="timeFreq"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
    
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList> 
      <!-- take double length as in mesh due to symmetry -->
      <var name="heightCoil" value="3e-3"/>
      <var name="lengthCoil" value="6e-3"/> 
      <var name="NumWindings" value="1000"/>
      <var name="AmplitudeCurrent" value="1.25"/>
      <var name="NumTimesteps" value="8"/>
      <var name="TimestepLength" value="3.5e-3"/>
    </variableList>
    <regionList>
      <region name="stripe" material="FECO_Sutor_Revised"/>
      <region name="coil" material="Air"/>
      <region name="yoke" material="Epcos_N87"/>
      <!--<region name="yoke" material="Air"/>-->
      <region name="air" material="Air"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="leftBoundary"/>
      <surfRegion name="rightBoundary"/>
      <surfRegion name="topBoundary"/>
      <surfRegion name="bottomBoundary"/>
    </surfRegionList>
    
    <elemList>
      <elems name="observerInner"/>
      <elems name="observerOuter"/>
      <elems name="observerLine"/>
    </elemList>
  </domain>
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>NumTimesteps</numSteps>
        <deltaT>TimestepLength</deltaT>
      </transient>
    </analysis>
    <pdeList>
      <magnetic>
        <regionList>
          <region name="yoke"/>
          <region name="air"/>
          <region name="coil"/>
          <!--<region name="stripe"/>-->
          <region name="stripe" nonLinIds="h"/>
        </regionList>
        
        <nonLinList>
          <hysteresis id="h"/>
        </nonLinList>
        
        <bcsAndLoads>
          <fluxParallel name="topBoundary">
            <comp dof="x"/>
          </fluxParallel>
          <fluxParallel name="bottomBoundary">
            <comp dof="x"/>
          </fluxParallel>
          <fluxParallel name="rightBoundary">
            <comp dof="y"/>
          </fluxParallel>
        </bcsAndLoads>
        
        <coilList>
          <coil id="excitation">
            <source type="current" value="AmplitudeCurrent*sample1D('inputCurrent.txt',t,1)"/>
            <part id="1">
              <regionList>
                <region name="coil"/>
              </regionList>
              <direction>
                <analytic coordSysId="default">
                  <comp dof="z" value="1"/>
                </analytic>
              </direction>
              <wireCrossSection area="heightCoil*lengthCoil/NumWindings"/>
            </part>
          </coil>
          
        </coilList>
        
        <storeResults>
          <elemResult type="magFieldIntensity">
            <allRegions/>
            <elemList>
              <elems name="observerInner" outputIds="txtEntity"/>
              <elems name="observerOuter" outputIds="txtEntity"/>
            </elemList>
          </elemResult>
          <elemResult type="magFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="observerInner" outputIds="txtEntity"/>
              <elems name="observerOuter" outputIds="txtEntity"/>
            </elemList>
          </elemResult>
          <elemResult type="magPolarization">
            <allRegions/>
            <elemList>
              <elems name="observerInner" outputIds="txtEntity"/>
              <elems name="observerOuter" outputIds="txtEntity"/>
            </elemList>
          </elemResult>
        </storeResults>
        
      </magnetic>      
    </pdeList>
    
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <!-- setup solution process for non-linear, hysteretic system -->
            <!-- minLoggingToTerminal = 0 > disable direct output of non-linear iterations;
              = 1 > write incremental, residual error, linesearch factor etc. to cout
              after each iteration	
              = 2 > as 1 but write out overview over all iterations at end of a timestep -->
            <hysteretic loggingToFile="yes" loggingToTerminal="2">
              <solutionMethod>
                <!-- Solve system using a quasi-newton scheme; here, the Jacobian matrix is approximated by 
                  - Finite Differences (on element level, global matrix is assemble from these local matrices);
                  - parameter IterationsTillUpdate can be used to set the update interval for the Jacobian approximation
                  (smallest value = 1 = update each iteration; largest value > maximal number of non-linear iterations;
                  in the later case, the Jacobian would only be computed once per timestep = Chord method);
                  - parameter initialNumberFPSteps can be used to jump-start iteration process with fixpoint iterations;
                  this can be quite useful for quasi-Newton methods as those are usually just locally convergent and thus
                  require a good starting point; during later iterations a linesearch method helps to stay in the local convergence
                  zone (hopefully) -->
                <QuasiNewton_FiniteDifferenceJacobian IterationsTillUpdate="1" initialNumberFPSteps="3"/>
              </solutionMethod>
              <lineSearch>
                <!-- a detailled description of the single line-search methods can be accessed over the tooltip help in oxygen editor -->
                <Backtracking_NonMonotonicArmijo>
                  <maxIterLS>15</maxIterLS>
                  <etaStart>1.0E0</etaStart>
                  <etaMin>1.0E-5</etaMin>
                  <decreaseFactor>5.0E-1</decreaseFactor>
                  <rho>9.0E-1</rho>
                  <historyLength>5</historyLength>
                </Backtracking_NonMonotonicArmijo>
              </lineSearch>
              <stoppingCriteria>
                <increment_relative value="2.5E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
                <residual_relative value="2.5E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
              </stoppingCriteria>
              <evaluationDepth>
                <evaluateAtIntegrationPoints_OneOperatorPerElement></evaluateAtIntegrationPoints_OneOperatorPerElement>
              </evaluationDepth>
              <failbackCriteria>
                <residual_relative value="1.0E-6" firstCheck="10" checkingFrequency="2" scaleToSystemSize="no"/>
                <increment_relative value="1.0E-6" firstCheck="10" checkingFrequency="2" scaleToSystemSize="no"/>
                <residual_absolute value="1.0E-9" firstCheck="3" checkingFrequency="3" scaleToSystemSize="yes"/>
              </failbackCriteria>
              <maxIter>30</maxIter>
              <!-- usually, the non-linear solution process starts from the solution of the last timestep; sometimes
                this value is too far off the solution of the current timestep, e.g., if the timestepsize is very coarse
                or the material has very sharp jumps; in those cases even an initial fixpoint iteration might not help to
                find a suitable starting point for quasi-Newton methods and a globally convergent fixpoint scheme would
                take way too long; in these cases it can help to reset the solution process and start from the zero vector;
                NOTE: this is a very brutal reset that is not really recommenable; use only if nothing else helps -->
<!--              <resetSolutionVectorToZero iterationsTillReset="25"/>-->
            </hysteretic>
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
        
  </sequenceStep>
  
</cfsSimulation>

